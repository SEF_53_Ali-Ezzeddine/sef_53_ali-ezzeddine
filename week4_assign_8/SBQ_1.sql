drop database DBPCLAIMS;
create database DBPCLAIMS;
use DBPCLAIMS;

CREATE TABLE claims (
    claim_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    patient_name VARCHAR(20) NOT NULL
);

DELIMITER @@
create trigger tr1claims before insert on claims for each row 
begin

if (new.patient_name=null) then

set new.patient_name='no name';

end if;
end;
@@ DELIMITER ;


insert into claims(claim_id,patient_name) values (1,'Bassem Dghaidi'),
 (2,'Omar Breidi'), (3,'Marwan Sawwan');

CREATE TABLE defendants (
    claim_id INT(8) NOT NULL,
    defendant_name VARCHAR(20) NOT NULL
);

DELIMITER @@
create trigger trdefendants before insert on defendants for each row 
begin

if (new.defendant_name=null) then

set new.defendant_name='no def name';

end if;
end;
@@ DELIMITER ;


insert into defendants(claim_id,defendant_name) values (1,'Jean Skaff'),
(1,'Radwan Sameh'),
(2,'Joseph Eid'),
(2,'Paul Syoufi'),
(2,'Radwan Sameh'),
(3,'Issam Awwad');


CREATE TABLE legalevents (
    claim_id INT(8) NOT NULL,
    defendant_name VARCHAR(20) NOT NULL,
    claim_status VARCHAR(6) NOT NULL,
    change_date DATE
);




insert into legalevents(claim_id,defendant_name,claim_status,change_date) 
values (1,'Jean Skaff','AP','2016-01-01'),
(1,'Jean Skaff','OR','2016-02-02'),
(1,'Jean Skaff','SF','2016-03-01'),
(1,'Jean Skaff','CL','2016-04-01'),
(1,'Radwan Sameh','AP','2016-01-01'),
(1,'Radwan Sameh','OR','2016-02-02'),
(1,'Radwan Sameh','SF','2016-03-01'),
(1,'Elie Meouchi','AP','2016-01-01'),
(1,'Elie Meouchi','OR','2016-02-02'),
(2,'Radwan Sameh','AP','2016-01-01'),
(2,'Radwan Sameh','OR','2016-02-01'),
(2,'Paul Syoufi','AP','2016-01-01'),
(3,'Issam Awwad','AP','2016-01-01');


CREATE TABLE claimstatuscodes (
    claim_status VARCHAR(6) NOT NULL,
    claim_status_desc VARCHAR(50),
    claim_seq INT(8) PRIMARY KEY
);


DELIMITER @@
create trigger aftrupdate after update on claimstatuscodes for each row 
begin

update  legalevents

set   claim_status=new.claim_status
where claim_status=new.claim_status; -- should be id 

end;
@@ DELIMITER ;


insert into claimstatuscodes(claim_status,claim_status_desc,claim_seq)
values ('AP','Awaiting review panel',1),
('OR','Panel opinion rendered',2),
('SF','Panel opinion rendered',3),
('CL','Closed',4);

