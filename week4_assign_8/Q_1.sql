use DBPCLAIMS;
SELECT 
    FResult.claim_id, FResult.patient_name, csc.claim_status
FROM
    (SELECT 
        Result.claim_id,
            Result.patient_name,
            MIN(statuscount) AS valuestat
    FROM
        (SELECT 
        LE.claim_id,
            CL.patient_name,
            LE.defendant_name,
            COUNT(LE.claim_status) AS statuscount
    FROM
        legalevents AS LE
    JOIN claims AS CL ON CL.claim_id = LE.claim_id
    GROUP BY LE.claim_id , CL.patient_name , LE.defendant_name) AS Result
    GROUP BY Result.claim_id) AS FResult
        JOIN
    claimstatuscodes AS csc ON csc.claim_seq = FResult.valuestat
ORDER BY FResult.claim_id

