use sakila;

SELECT 
    CT.first_name, COUNT(DISTINCT INV.film_id)
FROM
    customer AS CT
        JOIN
    rental AS RN ON RN.customer_id = CT.customer_id
        JOIN
    inventory AS INV ON INV.inventory_id = RN.inventory_id
GROUP BY CT.first_name
ORDER BY COUNT(INV.film_id) DESC
LIMIT 3