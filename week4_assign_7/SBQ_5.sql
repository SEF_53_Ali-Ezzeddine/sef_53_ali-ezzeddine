use sakila;

SELECT 
    AC.first_name, AC.last_name, FL.release_year,FL.description
FROM
    actor AS AC
        JOIN
    film_actor AS FA ON FA.actor_id = AC.actor_id
        JOIN
    film AS FL ON FL.film_id = FA.film_id
     

WHERE
    FL.description like'%Crocodile%shark%' or FL.description like'%shark%Crocodile%'
ORDER BY AC.last_name

