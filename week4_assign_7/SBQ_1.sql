SELECT 
    AC.actor_id,
    CONCAT(AC.first_name, '  ', AC.last_name) as 'name',
    COUNT(FA.film_id)  as 'film nb'
FROM
    film_actor AS FA
        JOIN
    actor AS AC ON AC.actor_id = FA.actor_id
GROUP BY AC.actor_id , AC.first_name

