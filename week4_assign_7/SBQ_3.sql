use sakila;

SELECT 
    CO.country, COUNT(CT.first_name) as 'name'
FROM
    customer AS CT
        JOIN
    address AS AD ON AD.address_id = CT.address_id
        JOIN
    city AS CI ON CI.city_id = AD.city_id
        JOIN
    country AS CO ON CO.country_id = CI.country_id
GROUP BY CO.country
ORDER BY COUNT(CT.customer_id) DESC
LIMIT 3;

