use sakila;

SELECT 
    MONTH(PY.payment_date) AS Mval,
    YEAR(PY.payment_date) AS Yval,
    ST.store_id AS stval,
    SUM(PY.amount),
    AVG(PY.amount)
FROM
    payment AS PY
        JOIN
    staff AS ST ON ST.staff_id = PY.staff_id
GROUP BY stval , Yval , Mval
ORDER BY Mval , Yval , stval