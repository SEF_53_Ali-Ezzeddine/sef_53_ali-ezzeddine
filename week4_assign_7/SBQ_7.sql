use sakila;

SELECT 
    CS.first_name, CS.last_name
FROM
    customer AS CS
WHERE
    CS.first_name IN (SELECT 
            first_name
        FROM
            actor
        WHERE
            actor_id = 8) 
UNION SELECT 
    AC.first_name, AC.last_name
FROM
    actor AS AC
WHERE
    AC.first_name IN (SELECT 
            first_name
        FROM
            actor
        WHERE
            actor_id = 8)
        AND AC.actor_id != 8