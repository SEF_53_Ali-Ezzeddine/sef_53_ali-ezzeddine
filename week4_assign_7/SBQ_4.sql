use sakila;

SELECT 
    AD.address, AD.address2
FROM
    address AS AD
WHERE
    AD.address2 IS NOT NULL
        AND AD.address2 NOT LIKE ''
ORDER BY AD.address2;