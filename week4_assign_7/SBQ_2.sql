use sakila;

SELECT 
    FL.language_id, COUNT(FL.title) as 'count'
FROM
    film AS FL
        JOIN
    language AS LG ON LG.language_id = FL.language_id
WHERE
    FL.release_year = '2006'
GROUP BY FL.language_id
ORDER BY COUNT(FL.title) DESC
LIMIT 3;

