
use sakila;

SELECT 
    FLC.category_id, CAT.name, COUNT(FLC.film_id)
FROM
    film_category AS FLC
        JOIN
    category AS CAT ON CAT.category_id = FLC.category_id
GROUP BY FLC.category_id , CAT.name

HAVING 

ifnull(COUNT(FLC.film_id) >= 55 and COUNT(FLC.film_id) <= 6,COUNT(FLC.film_id) > 65)

or

ifnull(COUNT(FLC.film_id) > 65,True)



        



ORDER BY COUNT(FLC.film_id) DESC




