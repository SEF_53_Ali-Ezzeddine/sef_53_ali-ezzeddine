<?php

	require_once 'database/Rapp.php';
	// get the HTTP method, path and body of the request
	$verb = $_SERVER['REQUEST_METHOD'];
	$method = $verb ;
	$request = explode('/', trim($_SERVER['PATH_INFO'], '/'));
	// getting variables added of the request
	$queryd = $_SERVER['QUERY_STRING'];
	// get ttable name the request
	$table = preg_replace('/[^a-z0-9_]+/i', '', array_shift($request));
	$key = array_shift($request);
	$data = explode(',', trim ($key, '{}'));
	// launching wrap class to check method if get or post ..
	$wrap = new Rapp();
	$wrap->wrap($method, $table, $data, $queryd);

?>