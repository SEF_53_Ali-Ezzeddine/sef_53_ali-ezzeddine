<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//Route::get('about', 'PagesController@getAbout');
//Route::get('contact', 'PagesController@getContact');
//Route::get('/', 'PagesController@getIndex');

//Route::POST('/posts', 'PostController@store');

Route::group(['middleware' => ['web']], function () {
    Route::get('contact', 'PagesController@getContact');
	Route::get('about', 'PagesController@getAbout');
	Route::get('/', 'PagesController@getIndex');
	Route::resource('posts', 'PostController');
Route::get('logout', 'LoginController@logout');

Auth::routes();

Route::get('/', function() {
	return redirect('/posts');
});



// Authentication Routes
//Route::get('auth/login', 'Auth\RegisterController@getLogin');
//Route::post('auth/login', 'Auth\RegisterController@postLogin');




// Registration Routes
//Route::get('auth/register', 'Auth\RegisterController@getRegister');
//Route::post('auth/register', 'Auth\RegisterController@postRegister');

});


?>
