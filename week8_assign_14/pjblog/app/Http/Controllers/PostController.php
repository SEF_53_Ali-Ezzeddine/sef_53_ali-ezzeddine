<?php


namespace pjblog\Http\Controllers;

use Illuminate\Http\Request;

use pjblog\Http\Requests;
use pjblog\Http\Controllers\Controller;
use pjblog\Post;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('id', 'desc')->paginate(5);

        return view("posts.index")->withPosts($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view("posts.create");

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


//DB::table('posts')->insert(
//   [`title` => 'wwwwww', `body` => 'Yes hel']);

//INSERT INTO `pjblog`.`posts` (`id`, `title`, `body`, `created_at`) VALUES ('1', 'Hel', 'Yes hel', '');
        $this->validate($request, array('title' => 'required|max:255',

                                       'body' => 'required'));

       $post = new Post;

       $post->title = $request->title;
       $post->body = $request->body;
       //$post->created_at = '2006-02-14 22:04:36';
        //$post->updated_at = '2006-02-14 22:04:36';

        $post->save();

       
    
        return redirect()->route('posts.show', $post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show')->withPost($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
