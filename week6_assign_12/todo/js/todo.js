
// Solution: Add interactivity so the user can manage daily tasks

var taskInput = document.getElementById("new-task");
var subjectInput = document.getElementById("new-task2");
var addButton = document.getElementsByTagName("button")[0];
var incompleteTasksHolder = document.getElementById("incomplete-tasks");


  //New Task List Item
  var createNewTaskElement = function(taskString) 
  {
      //Create List Item
     var listItem = document.createElement("li");
     //label
     var label = document.createElement("label");
    //input (text)
      //Each element needs modifying   
      label.innerText = taskString;   
      // each element needs appending
      listItem.appendChild(label);

      return listItem;
    
    }


//get values from local storage for view
var gettodoval = function() 
{

    var todos = new Array;
    var todos_str = localStorage.getItem('todo');
    //only if values exist
    if (todos_str !== null) {
        todos = JSON.parse(todos_str); 
    }
    return todos;
}


// Add a new task
var addTask = function() 
{
    //having date and time of addition 
    var newDate = new Date();
    var datetimeval = "added: "+ newDate.toString();
   //Create a new list item with the text title
    var taskin = taskInput.value.substr(0,30);
        //testing if value exist in title
        if (taskin.length > 0){

          var subjectval = subjectInput.value;
          //inserting title ,date, time and subject in array
          var resultValue = [taskin,datetimeval.substr(0,28),subjectval];
          var todos = gettodoval();
          //adding the array to local storage
          todos.push(resultValue);
          localStorage.setItem('todo', JSON.stringify(todos));
          //showing the new list
           showList();
           //Append listItem to incompleteTasksHolder 
          incompleteTasksHolder.appendChild(listItem); 

          taskInput.value = "";   
     }
     this.form.reset();
    }

showList();
 
      
// Set the click handler to the addTask function
addButton.addEventListener("click", addTask);

// deleting from list according to id of the button
function remove() 
{
    var id = this.getAttribute('id');
    var todoval = gettodoval();
    todoval.splice(id, 1);
    localStorage.setItem('todo', JSON.stringify(todoval));
 
    showList();
 
    return false;
}
 
function showList() 

{
    var todos = gettodoval();
 
    var html = '<ul id="tasks">';

       for(var i=0; i<todos.length; i++) {
            html += '<link rel="stylesheet" type="text/css" href="main.css" />'+'<li>' +'<div class="col1">'
            +'<div class="row1">'+ todos[i][0]+'</div>'+'<div class="row2">'+ todos[i][1]+'</div>'+'<div class="row3">'+
            todos[i][2]+'</div>'+'</div>'+'<div class="col2">' +'<hr class="ln" style="float: left;margin-left:0px; width: 1px; height: 70px;">' +
            '</div>'+'<div class="col3">' +'<button class="remove" id="' + i  + '" style="float: left; margin: 30px 0px 20px 5px;">x</button></li>';

      }

    html += '</ul>';
    document.getElementById('incomplete-tasks').innerHTML = html;
    
    var buttons = document.getElementsByClassName('remove');
    for (var i=0; i < buttons.length; i++) {
        buttons[i].addEventListener('click', remove);
    };
}


