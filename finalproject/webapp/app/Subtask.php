<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Task;
class Subtask extends Model
{
    

   	public function task() 
	{
		return $this->belongsTo(Task::class, 'id');
	}


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
