<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Subtask;
use App\User;
class Task extends Model
{

    public function subtasks()
    {
        $result = $this->hasMany(Subtask::class, 'task_id')
        		->select(['id', 'subtask_name','user_id', 'subtask_due', 'updated_at', 'task_status'])
                ->groupby('id');
        		$array = $result;
        return $array;		
    }

	public function user()
    {
        return $this->belongsTo(User::class, 'id');
    }

}