<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Task;
use App\Subtask;
use App\User;
use Session;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use DB;
class TaskController extends Controller
{
    public function index()
    {    
        $loggedUserId = Auth::id();
        $users = DB::table('users')
                ->get()
                ->where('id', $loggedUserId);
            foreach ($users as $user)
            {
                $user->userstatus;
            }
        $task = new Task; 
            if ( $user->userstatus != 1) 
            {
                $tasks = Task::orderBy('id','task_name')
                        ->where('user_id', $loggedUserId)
                        ->paginate(3);
            }else{
                $tasks = Task::orderBy('id','task_name')
                        ->paginate(3);
            }
        $subtasks = DB::table('subtasks')
                    ->get()
                    ->where('user_id', $loggedUserId);
                    // exclude extra details from friends table
                    $data = DB::table("subtasks")
                    ->select(DB::raw("SUM(task_status) as sumt"))
                    ->groupBy(DB::raw("task_id"))
                    ->get();
        $value = json_decode($data, true);
            if ( $user->userstatus != 1) 
            {        
                return  
                    view('tasks.index') 
                    ->with('storedTasks', $tasks)
                    ->with('sumt', $data );
            }else{
                return  
                    view('tasks.admin') 
                    ->with('storedTasks', $tasks)
                    ->with('sumt', $data );
            }
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        $this->validate($request, [
                'newTaskName' => 'required|min:5|max:255',
            ]);
        $data = $this->importCsv();
        for ($i = 0; $i < count($data); $i ++)
        {
            $task = new Task;
            $task->id =$data[$i]['id'];
            $task->task_name =$data[$i]['task_name'];
            $task->task_due ='2017-03-23 05:53:23';
            $task->task_status ='0';
            $task->user_id ='1';
            $task->save();
        }  
        Session::flash('success', 'New task has been succesfully added!');
        return redirect()
        ->route('tasks.index');   
    }
    public function show($task_id)
    {
        $task   = DB::table('tasks')
                    ->get();
        $subtask = DB::table('subtasks')
                    ->where('task_id', $task_id)
                    ->get();
        $this->get_data($task);
        return  view('tasks.calendar')
                ->with('task', $task)
                ->with('subtask', $subtask);
    }
    function get_data($inf) {
        $session = curl_init(); 
        $timeout = 20;
        curl_setopt($session, CURLOPT_URL, $inf);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($session, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($session, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($session);
        curl_close($session);
    }
    public function edit($task_id)
    {
        $this->importCsv();
        $task = Task::find($task_id);
        return view('tasks.edit')
               ->with('taskUnderEdit', $task);
    }
    public function update(Request $request, $task_id)
    {
        $this->validate($request, [
                'updatedTaskName' => 'required|min:5|max:255',
            ]);
        $task = Task::find($task_id);
        $data = DB::table("subtasks")
                ->select(DB::raw("SUM(task_status) as sumt"), 
                DB::raw("count(task_status) as countt"))
                ->where('task_id', $task_id)
                ->groupBy(DB::raw("task_id"))
                ->get();    
        $datasum = json_decode($data);
            if ($datasum[0]->sumt == $datasum[0]->countt)
            {
              $task->task_status =1;  
            }else{                
              $task->task_status =0;
            }
        $task->task_hint = $request->updatedTaskName;
        $task->save();
        Session::flash('success', 'Task #' . $task_id 
                . ' has been successfully updated.');
        return  redirect()
                ->route('tasks.index');
    }
    public function destroy($task_id)
    {
        $task = Task::find($task_id);
        $task->delete();
        Session::flash('success', 'Task #' . $task_id .
                    ' has been successfully deleted.');
        return redirect()
        ->route('tasks.index');
    }
    public function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
        {
        return false;
        }
        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }
    public function importCsv()
    {
        $file = public_path('file/data.csv');
        $customerArr = $this->csvToArray($file);
        return $customerArr;    
    }

}
