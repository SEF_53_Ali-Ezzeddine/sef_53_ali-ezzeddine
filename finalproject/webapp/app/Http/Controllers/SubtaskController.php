<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\Subtask;
use DB;

class SubtaskController extends Controller
{

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($subtask_id)
    {  
        switch ($subtask_id) {
            case "1":
                    $list = DB::table('policys')
                            ->join('procedures', 'policys.id', 
                                    '=', 'procedures.policy_id')  
                            ->groupby('policys.id','procedures.pc_name')
                            ->get(['policys.id','policys.pc_name as pc_name1', 
                                    'procedures.pc_name as pc_name2']);
                            return view('tasks.list')
                            ->with('list', $list);    
            break;
            case "2":
                    $listuser = DB::table('users')
                            ->get(['name as pc_name', 'user_info']);
                            return  view('tasks.listuser')
                            ->with('listuser', $listuser);  
            break;
            case "3":
                    $listuser = DB::table('users')
                            ->get(['name as pc_name', 'user_info']);
                            return  view('tasks.listuser')
                            ->with('listuser', $listuser);  
            break;

            case "5":
                    $listutable= DB::table('tables')
                            ->get();
                            return  view('tasks.listtable')
                            ->with('listtable', $listutable);    
            break;
        }
    }

    public function edit($subtask_id)
    {
        $subtask = Subtask::find($subtask_id);
        $subtask->task_status =1;
        $subtask->save();
        return redirect()->route('tasks.index');
    }

    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}

