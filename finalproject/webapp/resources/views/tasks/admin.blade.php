<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> -- Tasks -- </title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="css/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/startmin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/morris.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ url('/') }}">Home</a>
        </div>

        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <!-- Top Navigation: Left Menu -->
        <ul class="nav navbar-nav navbar-left navbar-top-links">
        </ul>
        <ul class="nav navbar-nav navbar-left navbar-top-links">
            <li><a href="{{ url('/home') }}"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</a></li>
        </ul>

        <!-- Top Navigation: Right Menu -->
        <ul class="nav navbar-right navbar-top-links">
            <li class="dropdown navbar-inverse">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-bell fa-fw"></i> <b class="caret"></b>
                </a>
                <ul class="dropdown-menu dropdown-alerts">
                    <li>
                        <a href="#">
                            <div>
                                <i class="fa fa-comment fa-fw"></i> New Comment
                                <span class="pull-right text-muted small"></span>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a class="text-center" href="#">            
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> Profile <b class="caret"></b>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li>                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                    Logout
                            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            <li>
            <!-- Authentication Links -->
            @if (Auth::guest())
                <li><a href="{{ route('login') }}">Login</a></li>
                <li><a href="{{ route('register') }}">Register</a></li>
            @else
            <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    {{ Auth::user()->name }}
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                    Logout
                            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
                        </li>
                    </ul>
            </li>
            @endif
            </li>

                    </li>
                </ul>
            </li>
        </ul>

        <!-- Sidebar -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">

                <ul class="nav" id="side-menu">
                    <li class="sidebar-search">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                        </div>
                    </li>                          
                    <li>
                        <a href="{{ route('tasks.show', '2') }}" class="active"><i class="fa fa-dashboard fa-fw"></i>Calendar</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-sitemap fa-fw"></i>Information<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{ route('subtasks.show', '3') }}">History</a>
                            </li>
                            <li>
                                <a href="#">List of information <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{ route('subtasks.show', '1') }}">All Policies-Procedures
                                        </a>

                                    </li>

                                    <li>
                                        <a href="{{ route('subtasks.show', '2') }}">Users</a>
                                        
                                    </li>
                                    <li>
                                        <a href="{{ route('subtasks.show', '4') }}">Appointments</a>
                                        
                                    </li>
                                      <li>
                                        <a href="{{ route('subtasks.show', '5') }}">Table Request</a>
                                        
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>

            </div>
        </div>
    </nav>
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"></h1>
                    <body>
    {{-- display success message --}}
    @if (Session::has('success'))
      <div class="alert alert-success">
        <strong>Success:</strong> {{ Session::get('success') }}
      </div>
    @endif

    {{-- display error message --}}
    @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Error:</strong>
        <ul>
          @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <div class="row" style='margin-top: 10px; margin-bottom: 10px;'>
      <form action="{{ route('tasks.store') }}" method='POST'>
      {{ csrf_field() }}
        <div class="col-md-1">
          <input type="submit" class='btn btn-primary btn-block' value='Csv'>
        </div>

      </form>
    </div>

    {{-- display stored tasks --}}
    @if (count($storedTasks) > 0)
    <table id="table" class="table">
<th class="hd">Id</th>
<th class="hd">Description </th>
<th class="hdpcount">Nb of Procedures</th>
</table>
          @foreach ($storedTasks as $storedTask)
             <td>
               <details>
               <table id="table" class="table">
              @foreach($storedTask->subtasks as $subvalue) 
              <form action="{{ route('subtasks.edit', $subvalue['subtask_id']) }}">

              <h3> <tr><td>
              <header class="pdate-detail">{{ $subvalue['subtask_due'] }}</header><br>
              {{ $subvalue['subtask_name'] }}<br>

              <p id="demo">
               {{  $subvalue['task_status'] == 1 ? 'Complete' : 'Incomplete' }}
              </p>
              <a href="{{ route('subtasks.edit', $subvalue['id']) }}">submit</a>
              </td></tr></h3> 
                </form>
                @endforeach
                </table>

                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
            </td> 
            <summary>
                        <header id="p-date" class="p-date"><header class="hd">Start Date
                        </header> 
                        {{ date($storedTask->created_at) }}
                                      <td><a href="{{ route('tasks.edit', ['tasks'=>$storedTask->id]) }}" class='btn btn-default' onclick="setTimeout(myFunction, 0);">
              <th>{{ $storedTask->task_status == 1 ? 'Complete' : 'Incomplete' }}</th>
                  </a>
              </td>
                        </header>
                        <p id="demo2"> </p>
<script>
//var timestamp = 1301090400,
//date = new Date(timestamp * 1000),
var date =  $("#demo3").text();
datevalues = [
   date.getFullYear(),
   date.getMonth()+1,
   date.getfDate(),
   date.getHours(),
   date.getMinutes(),
];
</script>
            <table class="tabletask">
              <th class="txtwd"> {{ $storedTask->id }}</th>
              <td class="txtwd">{{ $storedTask->task_name }}</td>
              <th class="proc-nb">
              {{ count($storedTask->subtasks)  }}</th>
                     <p class="phint">i<span class="span2">{{ $storedTask->task_info }}</span></p>
            </table>
</head>
<body>
            <script>
            $(document).ready(function(){
                $("button").onclick(function(){

              var x = document.getElementById("p-date").innerHTML;
            //var time = new Date(); 
            console.log(x);
            if (1 < 20) {
                document.getElementByClassName("hd").innerHTML = "Good day";
            } 
                $(".table").fadeIn("slow");
                });
            });
            </script>
                          <header class="p-edate"><header class="p-hint">
                          <header class="hd">your Hint 
                          </header>{{ $storedTask->task_hint }}</header>
                          <header class="hd">End Date 
                          </header>{{ $storedTask->task_due }}</header>
                          <td class="txtwd"><header class="hd"></header>
            </summary> 
             <script>
                function myFunction() {
                    alert('Task Complete');
                }
                function check() {
                    var value = document.getElementById("demo").innerHTML;
                    var value2 = document.getElementById("demo").value;
                    //console.log(value);
                    document.getElementById("myCheck").checked = true;
                }
            </script>
              <td>
                  <script type="text/javascript">
                      function showDiv() {
                        document.getElementById('show').style.display = "block";
                       }
                  </script>
              </td>
            </tr>
                  </table>
                        </details>
          @endforeach
    @endif
    <div class="row text-center">
      {{ $storedTasks->links() }}
    </div>

  </div>
</div>
<script src="https://www.gstatic.com/firebasejs/3.8.0/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyCIAOobzR2VMitzrCslmp9ySUfIDPOI_kc",
    authDomain: "task-391b9.firebaseapp.com",
    databaseURL: "https://task-391b9.firebaseio.com",
    projectId: "task-391b9",
    storageBucket: "task-391b9.appspot.com",
    messagingSenderId: "559365832139"
  };
  firebase.initializeApp(config);
</script>
</body>  <!-- ... Your content goes here ... -->
        </div>
    </div>
</div>
<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="js/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/startmin.js"></script>
</body>
</html>
