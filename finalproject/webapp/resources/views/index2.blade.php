

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" >
<head>    
   
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>  
    	<base href="https://demo.astemplates.com/joomla-templates/002057/index.php" />
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="Joomla Website template" />
	<meta name="generator" content="Joomla! - Open Source Content Management" />
	<title>Home</title>
	<link href="/joomla-templates/002057/index.php?format=feed&amp;type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0" />
	<link href="/joomla-templates/002057/index.php?format=feed&amp;type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0" />
	<link rel="stylesheet" href="/joomla-templates/002057/templates/as002057/css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="/joomla-templates/002057/templates/as002057/css/style.general.css" type="text/css" />
	<link rel="stylesheet" href="/joomla-templates/002057/templates/as002057/css/tmpl.default.css" type="text/css" />
	<link rel="stylesheet" href="/joomla-templates/002057/templates/as002057/css/media.1024.css" type="text/css" />
	<link rel="stylesheet" href="/joomla-templates/002057/templates/as002057/css/media.980.css" type="text/css" />
	<link rel="stylesheet" href="/joomla-templates/002057/templates/as002057/css/media.768.css" type="text/css" />
	<link rel="stylesheet" href="/joomla-templates/002057/templates/as002057/css/media.480.css" type="text/css" />
	<link rel="stylesheet" href="/joomla-templates/002057/templates/as002057/css/style.default.css" type="text/css" />
	<link rel="stylesheet" href="/joomla-templates/002057/media/com_phocagallery/css/main/phocagallery.css" type="text/css" />
	<link rel="stylesheet" href="/joomla-templates/002057/media/com_phocagallery/css/main/rating.css" type="text/css" />
	<link rel="stylesheet" href="/joomla-templates/002057/media/com_phocagallery/css/custom/default.css" type="text/css" />
	<link rel="stylesheet" href="/joomla-templates/002057/media/mod_phocagallery_image/css/phocagallery.css" type="text/css" />
	<link rel="stylesheet" href="/joomla-templates/002057/media/system/css/modal.css" type="text/css" />
	<link rel="stylesheet" href="https://demo.astemplates.com/joomla-templates/002057/templates/as002057/css/ext.artslider.css" type="text/css" />
	<link rel="stylesheet" href="https://demo.astemplates.com/joomla-templates/002057/modules/mod_as_menu/css/ext.default.css" type="text/css" />
	<script src="/joomla-templates/002057/media/jui/js/jquery.min.js" type="text/javascript"></script>
	<script src="/joomla-templates/002057/media/jui/js/jquery-noconflict.js" type="text/javascript"></script>
	<script src="/joomla-templates/002057/media/jui/js/jquery-migrate.min.js" type="text/javascript"></script>
	<script src="/joomla-templates/002057/media/system/js/caption.js" type="text/javascript"></script>
	<script src="/joomla-templates/002057/media/jui/js/bootstrap.js" type="text/javascript"></script>
	<script src="/joomla-templates/002057/templates/as002057/js/jquery.isotope.min.js" type="text/javascript"></script>
	<script src="/joomla-templates/002057/templates/as002057/js/touch.gallery.js" type="text/javascript"></script>
	<script src="/joomla-templates/002057/templates/as002057/js/scripts.js" type="text/javascript"></script>
	<script src="/joomla-templates/002057/media/system/js/mootools-core.js" type="text/javascript"></script>
	<script src="/joomla-templates/002057/media/system/js/core.js" type="text/javascript"></script>
	<script src="/joomla-templates/002057/media/system/js/mootools-more.js" type="text/javascript"></script>
	<script src="/joomla-templates/002057/media/system/js/modal.js" type="text/javascript"></script>
	<script src="/joomla-templates/002057/modules/mod_as_scroller/scripts/scroller.js" type="text/javascript"></script>
	<script src="https://demo.astemplates.com/joomla-templates/002057/modules/mod_as_artslider/js/camera.min.js" type="text/javascript"></script>
	<script src="https://demo.astemplates.com/joomla-templates/002057/modules/mod_as_artslider/js/easing-v1.3.js" type="text/javascript"></script>
	<script src="https://demo.astemplates.com/joomla-templates/002057/modules/mod_as_menu/js/script.js" type="text/javascript"></script>
	<script src="https://demo.astemplates.com/joomla-templates/002057/modules/mod_as_menu/js/jquery.mobilemenu.js" type="text/javascript"></script>
	<script src="https://demo.astemplates.com/joomla-templates/002057/modules/mod_as_menu/js/jquery.hovermenu.js" type="text/javascript"></script>
	<script type="text/javascript">
jQuery(window).on('load',  function() {
				new JCaption('img.caption');
			});
jQuery.noConflict()
		jQuery(function($) {
			SqueezeBox.initialize({});
			SqueezeBox.assign($('a.pg-modal-button').get(), {
				parse: 'rel'
			});
		});

		window.jModalClose = function () {
			SqueezeBox.close();
		};
		
		// Add extra modal close functionality for tinyMCE-based editors
		document.onreadystatechange = function () {
			if (document.readyState == 'interactive' && typeof tinyMCE != 'undefined' && tinyMCE)
			{
				if (typeof window.jModalClose_no_tinyMCE === 'undefined')
				{	
					window.jModalClose_no_tinyMCE = typeof(jModalClose) == 'function'  ?  jModalClose  :  false;
					
					jModalClose = function () {
						if (window.jModalClose_no_tinyMCE) window.jModalClose_no_tinyMCE.apply(this, arguments);
						tinyMCE.activeEditor.windowManager.close();
					};
				}
		
				if (typeof window.SqueezeBoxClose_no_tinyMCE === 'undefined')
				{
					if (typeof(SqueezeBox) == 'undefined')  SqueezeBox = {};
					window.SqueezeBoxClose_no_tinyMCE = typeof(SqueezeBox.close) == 'function'  ?  SqueezeBox.close  :  false;
		
					SqueezeBox.close = function () {
						if (window.SqueezeBoxClose_no_tinyMCE)  window.SqueezeBoxClose_no_tinyMCE.apply(this, arguments);
						tinyMCE.activeEditor.windowManager.close();
					};
				}
			}
		};
		
function addLoadEvent(func){if(typeof window.addEvent=='function'){window.addEvent('load',function(){func()});}else if(typeof window.onload!='function'){window.onload=func;}else{var oldonload=window.onload;window.onload=function(){if(oldonload){oldonload();}func();}}}
addLoadEvent(function(){marqueeInit({uniqueid: 'as_scroller_191',style:{'width':'100%','height':'100px'},inc:1,mouse:'pause',direction:'left',valign:'top',moveatleast: 1 ,neutral:0, savedirection:true});});
	</script>
	<style type="text/css">
.pg-cv-box-mod-ri {
   height: 90px;
   width: 50px;"
}
.pg-cv-box-img-mod-ri {
   height: 50px;
   width: 50px;"
}
</style>


	

<link href='http://fonts.googleapis.com/css?family=Open+Sans&subset=latin,greek,cyrillic' rel='stylesheet' type='text/css'><style type="text/css">

/***************************************************************************************/
/*
/*		Designed by 'AS Designing'
/*		Web: http://www.asdesigning.com
/*		Web: http://www.astemplates.com
/*		License: ASDE Commercial
/*
/**************************************************************************************/

body
{
	font-family: Tahoma, Geneva, sans-serif, Arial;	font-size: 12px;	}

a, #featured-row .mod-newsflash .item:hover h2
{
	}

a:hover,
a.selected
{
	}


/**************************************************************************************/
/*   Forms																			  */


input,
button,
select,
textarea
{
	font-family: Tahoma, Geneva, sans-serif, Arial;}


/**************************************************************************************/
/*   Headings and Titles															  */


h1,
h2,
h3,
h4,
h5,
.item_header .item_title
{
    font-family: 'Open Sans', Arial, serif !important;}

h1
{
		}

h2
{
		}

h3,
.item_header .item_title
{
		}

h4
{
			
}

h5
{
			
}


/**************************************************************************************/
/*   Lists																			  */


.categories-module li a,
.archive-module li a
{
	}

.categories-module li a:hover,
.archive-module li a:hover
{
	}


/**************************************************************************************/
/*   Logo Row		  																  */


#header-row .moduletable.call-now
{
	}

#header-row .moduletable.call-now div
{
    font-family: 'Open Sans', Arial, serif !important;}

#header-row .logo
{
	float: left;
	line-height: 60px;
	min-width: 240px;
}

#header-row .logo,
#header-row .logo a,
#header-row .logo a:hover
{
	font-family: 'Open Sans', Arial, serif !important;		font-style: normal;	font-weight: normal;	}

#header-row .logo span.slogan
{
	left: 5px;
	top: 0px;
	font-family: Arial, Helvetica, sans-serif;	font-size: 12px;	font-style: normal;	font-weight: normal;		
}


/**************************************************************************************/
/*   Footer
/**************************************************************************************/
/**************************************************************************************/


#footer-row ul.nav li a
{
	}

#footer-row ul.nav li a:hover
{
	}

#copyright-menu li a,
#copyright-menu li.current a,
#copyright-menu li.active a
{
	}

#copyright-menu li a:hover
{
	}


</style>
    <link rel="stylesheet" href="/joomla-templates/002057/templates/as002057/css/ext.asmenu.css" type="text/css" />
    <link rel="stylesheet" href="/joomla-templates/002057/templates/as002057/css/style.custom.css" type="text/css" />
  
</head>

<body class="com_content view-featured task- itemid-435 body-">
    <div class="wrapper">

		<!-- HEADER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  -->    
	    <div id="header-top-wrapper">
                        <div id="header-top-row">
                <div class="container">
                    <div id="top" class="row">
                        <div class="moduletable social  span12"><div class="mod-menu">
	<ul class="nav menu ">
	<li class="item-557"><a class="facebook" href="http://www.facebook.com" ><img src="/joomla-templates/002057/images/sampledata/asimages/social/facebook.png" alt="Facebook" /></a></li><li class="item-558"><a href="http://www.google.com" ><img src="/joomla-templates/002057/images/sampledata/asimages/social/google.png" alt="Google" /></a></li><li class="item-559"><a href="http://www.twitter.com" ><img src="/joomla-templates/002057/images/sampledata/asimages/social/twitter.png" alt="Twitter" /></a></li><li class="item-560"><a href="http://www.vimeo.com" ><img src="/joomla-templates/002057/images/sampledata/asimages/social/vimeo.png" alt="Vimeo" /></a></li><li class="item-561"><a href="http://www.rss.com" ><img src="/joomla-templates/002057/images/sampledata/asimages/social/rss.png" alt="Rss" /></a></li></ul>
</div>
</div>
                    </div>
                </div>
            </div>
                         
               
            <div id="header-row">
                <div class="container">
                    <div class="row">
                        <header>
                            <div id="logo" class="span4">
                                                                <a href="/joomla-templates/002057">
                                    <img src="/joomla-templates/002057/templates/as002057/images/logo.png" alt="TECHNOWEB" />
                                </a>
                                <span class="slogan">
                                                                    </span>
                                                            
                            </div>
                            
                            <div class="moduletable navigation  span8">

<style type="text/css">

#as-menu,
#as-menu ul.as-menu li
{
	background-color: #;
	border-radius: 0px;
	
	}

#as-menu ul.as-menu > li > a,
#as-menu ul.as-menu > li > span
{
	font-size: 12px;
	2px;	font-family: Tahoma, Geneva, sans-serif, Arial;	color: #;
}

#as-menu ul.as-menu > li.active > a,
#as-menu ul.as-menu > li.asHover > a,
#as-menu ul.as-menu > li.current > a,
#as-menu ul.as-menu > li.active > span,
#as-menu ul.as-menu > li.asHover > span,
#as-menu ul.as-menu > li.current > span,
#as-menu ul.as-menu > li > a:hover,
#as-menu ul.as-menu > li > span:hover,
#as-menu ul.as-menu ul li a:hover,
#as-menu ul.as-menu ul li span:hover,
#as-menu ul.as-menu ul li.active > a,
#as-menu ul.as-menu ul li.asHover > a,
#as-menu ul.as-menu ul li.active > span,
#as-menu ul.as-menu ul li.asHover > span
{
	color: #;
}

#as-menu ul.as-menu ul
{
	width: 191px;
	-webkit-border-radius: 0px;
	-moz-border-radius: 0px;
	border-radius: 0px;	
	-webkit-border-bottom-right-radius: 0px;
	-moz-border-radius-bottomright: 0px;
	border-bottom-right-radius: 0px;
	-webkit-border-bottom-left-radius: 0px;
	-moz-border-radius-bottomleft: 0px;
	border-bottom-left-radius: 0px;
}

#as-menu ul.as-menu ul li a,
#as-menu ul.as-menu ul li span
{
	font-size: 12px;
	2px;	font-family: Tahoma, Geneva, sans-serif, Arial;	color: #;
}

#as-menu ul.as-menu li li:hover ul,
#as-menu ul.as-menu li li.asHover ul,
#as-menu ul.as-menu li li li:hover ul,
#as-menu ul.as-menu li li li.asHover ul
{
	left: 191px;
}


</style>
<!--[if (gt IE 9)|!(IE)]><!-->
<script type="text/javascript">
    jQuery(function(){
        jQuery('.as-menu').mobileMenu({});
    })
</script>
<!--<![endif]-->


<div id="as-menu" class="menu-wrapper">
	
    <ul class="as-menu "  >
    
    <li class="item-435 current active"><a  href="/joomla-templates/002057/index.php" >Home</a></li><li class="item-489 deeper parent"><a  href="/joomla-templates/002057/index.php/blog" >Blog</a><ul><li class="item-492"><a  href="/joomla-templates/002057/index.php/blog/blog-1" >Blog 1 Column</a></li><li class="item-490"><a  href="/joomla-templates/002057/index.php/blog/blog-2" >Blog 2 Columns</a></li><li class="item-491"><a  href="/joomla-templates/002057/index.php/blog/blog-3" >Blog 3 Columns</a></li></ul></li><li class="item-488 deeper parent"><a  href="/joomla-templates/002057/index.php/about-us" >About Us</a><ul><li class="item-493"><a  href="/joomla-templates/002057/index.php/about-us/our-stuff" >Our Stuff</a></li><li class="item-556"><a  href="/joomla-templates/002057/index.php/about-us/our-stuff-2" >Our Services</a></li><li class="item-494"><a  href="/joomla-templates/002057/index.php/about-us/testimonials" >Testimonials</a></li></ul></li><li class="item-537"><a  href="/joomla-templates/002057/index.php/careers" >Careers</a></li><li class="item-487"><a  href="/joomla-templates/002057/index.php/contact-us" >Contact Us</a></li>    </ul>
</div>


<script type="text/javascript">
	jQuery(function(){
		jQuery('ul.as-menu').asmenu({
			hoverClass:    'asHover',         
		    pathClass:     'overideThisToUse',
		    pathLevels:    1,    
		    delay:         500, 
		    speed:         'normal',   
		    autoArrows:    false, 
		    dropShadows:   true, 
		    disableHI:     false, 
		    onInit:        function(){},
		    onBeforeShow:  function(){},
		    onShow:        function(){},
		    onHide:        function(){}
		});
	});
</script></div>
                        </header>
                    </div>
                </div>
            </div>
        
                        <div id="slider-row">
                <div class="_container">
                    <div class="row">
                        <div class="moduletable slider ">
<style type="text/css">

.slide_title
{
    color: #FFFFFF;
}

.slide_title a
{
    color: #FFFFFF;
}

.slide_title a:hover
{
    color: #;
}

.slide_title .item_title_part0 
{
    color: #0682B8;	
}

.slide_title .item_title_part1
{
    color: #0682B8;	
}

.camera_caption p
{
    color: #121212;
	font-size: 18px;
}

.camera_caption a.readmore
{
	background-color: #FFFFFF;
	color: #FFFFFF;
	
	}

.camera_caption a.readmore:hover
{
	background-color: #FFFFFF;
}

.camera_wrap .camera_pag .camera_pag_ul li.cameracurrent, 
.camera_wrap .camera_pag .camera_pag_ul li:hover
{
	background-color: #;	
	-webkit-border-radius: 10px;		
	-moz-border-radius: 10px;		
	border-radius: 10px;	
}

.camera_wrap .camera_pag .camera_pag_ul li
{
	background-color: #FFFFFF;
	-webkit-border-radius: 10px;		
	-moz-border-radius: 10px;		
	border-radius: 10px;		
}

</style>
<div id="camera-slideshow" class="slider">


<div class="camera-item" data-src="https://demo.astemplates.com/joomla-templates/002057/images/sampledata/asimages/slider/slide1.jpg">

		
		<div class="camera_caption fadeIn">
			
							<h2 class="slide_title slide_title__slider">
									<span class="item_title_part0">We</span> <span class="item_title_part1">Have</span> 								</h2>
				
				
							
			<p>propositions for everybody!</p>
	
					</div>
	
</div>

<div class="camera-item" data-src="https://demo.astemplates.com/joomla-templates/002057/images/sampledata/asimages/slider/slide2.jpg">

		
		<div class="camera_caption fadeIn">
			
							<h2 class="slide_title slide_title__slider">
									<span class="item_title_part0">There</span> <span class="item_title_part1">Are</span> 								</h2>
				
				
							
			<p>no complex tasks for us!</p>
	
					</div>
	
</div>

<div class="camera-item" data-src="https://demo.astemplates.com/joomla-templates/002057/images/sampledata/asimages/slider/slide3.jpg">

		
		<div class="camera_caption fadeIn">
			
							<h2 class="slide_title slide_title__slider">
									<span class="item_title_part0">We</span> <span class="item_title_part1">Will</span> 								</h2>
				
				
							
			<p>show the way to success!</p>
	
					</div>
	
</div>
</div>


<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('#camera-slideshow').camera({
			alignment			: "centerLeft",
			autoAdvance			: 1,
			mobileAutoAdvance	: 0,
			cols				: 6,
			easing				: "easeInOutExpo",
			mobileEasing		: "easeInOutExpo",
			fx					: "simpleFade",	
			mobileFx			: "simpleFade",
			gridDifference		: 2500,
			height				: "28%",
			imagePath			: 'images/',
			hover				: 1,
			navigation			: 1,
			navigationHover		: 1,
			mobileNavHover		: 1,
			opacityOnGrid		: 0,
			pagination			: 1,
			playPause			: 0,
			pauseOnClick		: 0,
			rows				: 4,
			slicedCols			: 6,
			slicedRows			: 4,
			time				: 5000,
			transPeriod			: 2000,
			onEndTransition		: function() {  },	//this callback is invoked when the transition effect ends
			onLoaded			: function() {  },	//this callback is invoked when the image on a slide has completely loaded
			onStartLoading		: function() {  },	//this callback is invoked when the image on a slide start loading
			onStartTransition	: function() {  }	//this callback is invoked when the transition effect starts
		});
	});
</script>
</div>
                    </div>
                </div>
            </div>
            		</div>
		
		<div id="custom-content-wrapper">
                            
                <div id="featured-row">
	        <div class="featured-row-wrapper">        
                <div class="container">
                    <div class="row">
                        <div class="moduletable   span3"><div class="mod-newsflash mod-newsflash__">
<div class="item">



<p><img src="/joomla-templates/002057/images/sampledata/asimages/img.home3.png" alt="" /></p>
<h3>IT Solutions</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec facilisis consequat tellus a fermentum. Maecenas convallis.</p>

<a class="readmore" href="/joomla-templates/002057/index.php/41-it-solution-p6/125-web-analytics">Read more ...</a></div>	
</div>
</div><div class="moduletable   span3"><div class="mod-newsflash mod-newsflash__">
<div class="item">



<p><img src="/joomla-templates/002057/images/sampledata/asimages/img.home2.png" alt="" /></p>
<h3>Web Analytics</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec facilisis consequat tellus a fermentum. Maecenas convallis.</p>

<a class="readmore" href="/joomla-templates/002057/index.php/42-web-analytics-p6/120-web-analytics">Read more ...</a></div>	
</div>
</div><div class="moduletable   span3"><div class="mod-newsflash mod-newsflash__">
<div class="item">



<p><img src="/joomla-templates/002057/images/sampledata/asimages/img.home6.png" alt="" /></p>
<h3>Development</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec facilisis consequat tellus a fermentum. Maecenas convallis.</p>

<a class="readmore" href="/joomla-templates/002057/index.php/62-web-development-p6/11-web-development">Read more ...</a></div>	
</div>
</div><div class="moduletable   span3"><div class="mod-newsflash mod-newsflash__">
<div class="item">



<p><img src="/joomla-templates/002057/images/sampledata/asimages/img.home4.png" alt="" /></p>
<h3>Support</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec facilisis consequat tellus a fermentum. Maecenas convallis.</p>

<a class="readmore" href="/joomla-templates/002057/index.php/44-technical-support-p6/13-technical-support">Read more ...</a></div>	
</div>
</div>
                    </div>
                </div>
            </div>
        </div>
            
    	    
                <div id="header-bottom-row">
	        <div class="header-bottom-row-wrapper">        
                <div class="container">
                    <div class="row">
                        <div class="moduletable   span12">
<div class="mod-single-article mod-single-article__">
	<div class="item item__module">

		<!-- Intro Image -->
											<div class="item_img img-intro img-intro__left"> <img src="/joomla-templates/002057/images/sampledata/asimages/welcome.png" alt=""/> </div>
					
		<!-- Item Title -->
					<h1 class="item_title">
									Welcome to TechnoWeb							</h1>
		
		
		

		<!-- Publish Date -->
		
		
		<!-- Intro Text -->

		<div class="item_introtext">
			<p>Maecenas sit amet tincidunt elit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nulla facilisi. Nulla facilisi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas</p>
<ul>
<li>Class aptent taciti sociosqu ad litora torquent per conubia nostra</li>
<li>Sed ut perspiciatis unde omnis istes err sit voluptatem accusantium</li>
<li>Totam rem aperiam, eaque ipsa uae ab illo inventore veritatis et quasi</li>
<li>Aliquam ac egestas dui. Vestibulum quis magna eu elit mollis cursus vel</li>
<li>Mauris consectetur, elit ac faucibus commodo, ante dolor consectetur liber</li>
</ul>

			<a class="btn btn_info readmore" href="/joomla-templates/002057/index.php/16-about-us/126-welcome-to-technoweb">Read More</a>		</div>	

	</div>
</div></div>
                    </div>
                </div>
            </div>
        </div>
        		<!-- END OF HEADER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->        
    
	    <!-- CONTENT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
        <div id="content-row">
            <div class="container">
                <div class="content-inner row">
                
                    <!-- COLUMN LEFT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                                        <!-- END OF COlUMN LEFT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                            
                    <!-- COLUMN MAIN ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->                
                    <div id="component" class="span12">
                
                                                                    
                        <div id="system-message-container">
	</div>

                        <div class="page-featured page-featured__">

	
		
		<div class="items-row cols-2 row-0 row-fluid">
					<div class="item item__featured column-1 span6">
				
	
	
			<div class="item_header">
			<h2 class="item_title">
								About Us							</h2>
		</div>
	
	
	
		

			
	 <p style="text-align: right;">Suspendisse mollis lectus libero, sed cursus lorem posuere sed. Sed felis est, consequat ac ornare quis, scelerisque sed risus. Curabitur scelerisque tellus et erat pulvinar venenatis. In suscipit nec mauris eu varius.</p>
<p style="text-align: right;">Vestibulum vel velit aliquam, placerat odio ut, suscipit ligula. Fusce eu urna et mauris vestibulum laoreet ac a eros. Donec sem mauris, hendrerit sit amet libero in, laoreet pellentesque lectus.</p>
<p style="text-align: right;">Maecenas vitae dui bibendum justo mollis tincidunt id molestie nibh. Mauris sem enim, gravida sit amet est in, semper pulvinar purus. Nulla facilisi. Aliquam a faucibus augue, eu viverra enim.</p>
<p style="text-align: right;">Suspendisse condimentum placerat mi, id varius ante auctor et. Duis egestas metus ac aliquam imperdiet. In eu pellentesque dui. Sed in luctus nisl. Duis sit amet faucibus urna, vitae facilisis leo.</p>
<p style="text-align: right;">Phasellus eleifend bibendum ligula euismod sagittis. Mauris tellus felis, dapibus vel dolor ac, rutrum pulvinar urna. Vestibulum nec auctor quam, ac volutpat metus. Morbi pretium, augue in posuere vehicula, mauris nulla faucibus enim, eget vehicula erat arcu id sem.</p>	
						<div class="clearfix"></div>
			</div>
			
			
	
					<div class="item item__featured column-2 span6">
				
	
	
	
	
	
		

			
	 <h2 class="advantage_1">Morbi porttitor pharetra</h2>
<p class="advantage_1">Morbi porttitor pharetra libero et semper. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra.</p>
<h2 class="advantage_2">Suspendisse mollis lectus</h2>
<p class="advantage_2">Suspendisse mollis lectus libero, sed cursus lorem posuere sed. Sed felis est, consequat ac ornare quis, scelerisque sed risus. Curabitur scelerisque tellus et erats.</p>
<h2 class="advantage_3">Sed vitae quam velit</h2>
<p class="advantage_3">Sed vitae quam velit. Morbi pellentesque velit a arcu vulputate elementum. Curabitur semper, dolor ut elementum feugiat, nulla arcu laoreet justot.</p>
<h2 class="advantage_4">Phasellus eleifend</h2>
<p class="advantage_4">Phasellus eleifend bibendum ligula euismod sagittis. Mauris tellus felis, dapibus vel dolor ac, rutrum pulvinar urna. Vestibulum nec auctor quam, ac volutpat metus.</p>	
						<div class="clearfix"></div>
			</div>
			
			
		</div>
		
	


</div>

                    
                                                
                    </div>
        			<!-- END OF COLUMN MAIN ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    
			        <!-- COLUMN RIGHT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->                    
                                        <!-- END OF COLUMN RIGHT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                </div>
            </div>
        </div>
                
                
                <div id="content-row-5">
	        	<div class="content-row-5-wrapper">
            <div class="container">
		            <div class="row">
        	            <div class="moduletable   span12"><h2 class="module_title ">What We Can Do For You</h2>

<div class="mod-custom mod-custom__"  >
	<p>Suspendisse mollis lectus libero, sed cursus lorem posuere sed.</p></div>
</div><div class="moduletable box  span4"><div class="mod-newsflash mod-newsflash__box">
<div class="item">



<p><img src="/joomla-templates/002057/images/sampledata/asimages/ico.monitoring.png" alt="" /></p>
<h3>Monitoring</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec facilisis consequat tellus a fermentum. Maecenas convallis.</p>

<a class="readmore" href="/joomla-templates/002057/index.php/69-pos24-monitoring/139-monitoring">Read more ...</a></div>	
</div>
</div><div class="moduletable box  span4"><div class="mod-newsflash mod-newsflash__box">
<div class="item">



<p><img src="/joomla-templates/002057/images/sampledata/asimages/ico.optimization.png" alt="" /></p>
<h3>Optimization</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec facilisis consequat tellus a fermentum. Maecenas convallis.</p>

<a class="readmore" href="/joomla-templates/002057/index.php/70-pos24-optimization/140-optimization">Read more ...</a></div>	
</div>
</div><div class="moduletable box  span4"><div class="mod-newsflash mod-newsflash__box">
<div class="item">



<p><img src="/joomla-templates/002057/images/sampledata/asimages/ico.copywriting.png" alt="" /></p>
<h3>Copywriting</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec facilisis consequat tellus a fermentum. Maecenas convallis.</p>

<a class="readmore" href="/joomla-templates/002057/index.php/71-pos24-copywriting/141-copywriting">Read more ...</a></div>	
</div>
</div>
            	    </div>
                </div>
            </div>
        </div>
               
        
                <div id="content-row-6">
	        	<div class="content-row-6-wrapper">
            <div class="container">
		            <div class="row">
        	            <div class="moduletable   span12"> 
<style type="text/css">


</style>
<link rel="stylesheet" href="https://demo.astemplates.com/joomla-templates/002057//modules/mod_as_scroller/css/styles.css" type="text/css" />

<div id="mod_as_scroller">
    <div class="as_marquee" id="as_scroller_191">
        <a href="/joomla-templates/002057/" ><img src="/joomla-templates/002057/images/sampledata/asimages/partners/partners_1.png" alt="" style="margin-right:15px" /></a><a href="/joomla-templates/002057/" ><img src="/joomla-templates/002057/images/sampledata/asimages/partners/partners_2.png" alt="" style="margin-right:15px" /></a><a href="/joomla-templates/002057/" ><img src="/joomla-templates/002057/images/sampledata/asimages/partners/partners_3.png" alt="" style="margin-right:15px" /></a><a href="/joomla-templates/002057/" ><img src="/joomla-templates/002057/images/sampledata/asimages/partners/partners_4.png" alt="" style="margin-right:15px" /></a><a href="/joomla-templates/002057/" ><img src="/joomla-templates/002057/images/sampledata/asimages/partners/partners_5.png" alt="" style="margin-right:15px" /></a><a href="/joomla-templates/002057/" ><img src="/joomla-templates/002057/images/sampledata/asimages/partners/partners_6.png" alt="" style="margin-right:15px" /></a><a href="/joomla-templates/002057/" ><img src="/joomla-templates/002057/images/sampledata/asimages/partners/partners_7.png" alt="" style="margin-right:15px" /></a><a href="/joomla-templates/002057/" ><img src="/joomla-templates/002057/images/sampledata/asimages/partners/partners_8.png" alt="" style="margin-right:15px" /></a><a href="/joomla-templates/002057/" ><img src="/joomla-templates/002057/images/sampledata/asimages/partners/partners_9.png" alt="" style="margin-right:15px" /></a>    </div>
</div>
</div>
            	    </div>
                </div>
            </div>
        </div>
                        
        
        <!-- FOOTER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
        <div id="footer">
                        <div id="footer-row">
                <div class="container">
                    <div class="row">
                                                <div class="footer-row-1">
                            <div class="moduletable   span6"><h3 class="module_title ">Recent Projects</h3><div id ="phocagallery-module-ri" style="text-align:center; margin: 0 auto;"><div class="mosaic" style="float:left;padding:0px;width:80px">
<a class="pg-modal-button" title="project.1" href="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_l_project.1.jpg" rel="{handler: 'image', size: {x: 200, y: 150}, overlayOpacity: 0.3}" >
<img src="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_s_project.1.jpg" alt="project.1" width="80" height="70" /></a></div><div class="mosaic" style="float:left;padding:0px;width:80px">
<a class="pg-modal-button" title="project.2" href="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_l_project.2.jpg" rel="{handler: 'image', size: {x: 200, y: 150}, overlayOpacity: 0.3}" >
<img src="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_s_project.2.jpg" alt="project.2" width="80" height="70" /></a></div><div class="mosaic" style="float:left;padding:0px;width:80px">
<a class="pg-modal-button" title="project.3" href="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_l_project.3.jpg" rel="{handler: 'image', size: {x: 200, y: 150}, overlayOpacity: 0.3}" >
<img src="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_s_project.3.jpg" alt="project.3" width="80" height="70" /></a></div><div class="mosaic" style="float:left;padding:0px;width:80px">
<a class="pg-modal-button" title="project.4" href="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_l_project.4.jpg" rel="{handler: 'image', size: {x: 200, y: 150}, overlayOpacity: 0.3}" >
<img src="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_s_project.4.jpg" alt="project.4" width="80" height="70" /></a></div><div class="mosaic" style="float:left;padding:0px;width:80px">
<a class="pg-modal-button" title="project.5" href="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_l_project.5.jpg" rel="{handler: 'image', size: {x: 200, y: 150}, overlayOpacity: 0.3}" >
<img src="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_s_project.5.jpg" alt="project.5" width="80" height="70" /></a></div><div class="mosaic" style="float:left;padding:0px;width:80px">
<a class="pg-modal-button" title="project.6" href="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_l_project.6.jpg" rel="{handler: 'image', size: {x: 200, y: 150}, overlayOpacity: 0.3}" >
<img src="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_s_project.6.jpg" alt="project.6" width="80" height="70" /></a></div><div class="mosaic" style="float:left;padding:0px;width:80px">
<a class="pg-modal-button" title="project.7" href="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_l_project.7.jpg" rel="{handler: 'image', size: {x: 200, y: 150}, overlayOpacity: 0.3}" >
<img src="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_s_project.7.jpg" alt="project.7" width="80" height="70" /></a></div><div class="mosaic" style="float:left;padding:0px;width:80px">
<a class="pg-modal-button" title="project.8" href="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_l_project.8.jpg" rel="{handler: 'image', size: {x: 200, y: 150}, overlayOpacity: 0.3}" >
<img src="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_s_project.8.jpg" alt="project.8" width="80" height="70" /></a></div><div class="mosaic" style="float:left;padding:0px;width:80px">
<a class="pg-modal-button" title="project.9" href="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_l_project.9.jpg" rel="{handler: 'image', size: {x: 200, y: 150}, overlayOpacity: 0.3}" >
<img src="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_s_project.9.jpg" alt="project.9" width="80" height="70" /></a></div><div class="mosaic" style="float:left;padding:0px;width:80px">
<a class="pg-modal-button" title="project.10" href="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_l_project.10.jpg" rel="{handler: 'image', size: {x: 200, y: 150}, overlayOpacity: 0.3}" >
<img src="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_s_project.10.jpg" alt="project.10" width="80" height="70" /></a></div><div class="mosaic" style="float:left;padding:0px;width:80px">
<a class="pg-modal-button" title="project.11" href="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_l_project.11.jpg" rel="{handler: 'image', size: {x: 200, y: 150}, overlayOpacity: 0.3}" >
<img src="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_s_project.11.jpg" alt="project.11" width="80" height="70" /></a></div><div class="mosaic" style="float:left;padding:0px;width:80px">
<a class="pg-modal-button" title="project.12" href="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_l_project.12.jpg" rel="{handler: 'image', size: {x: 200, y: 150}, overlayOpacity: 0.3}" >
<img src="/joomla-templates/002057/images/phocagallery/thumbs/phoca_thumb_s_project.12.jpg" alt="project.12" width="80" height="70" /></a></div></div><div style="clear:both"></div></div><div class="moduletable   span3"><h3 class="module_title ">People are Talking</h3>

<div class="mod-custom mod-custom__"  >
	<div class="blockquote"> </div>
<p style="padding-right: 40px;">Proin sodales auctor ligula eget hendrerit fermentum fermentum. Morbi porttitor pharetra libero et semper. Lorem ipsum dolor sit amet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin sodales auctor ligula eget hendrerit fermentum</p></div>
</div><div class="moduletable   span3"><h3 class="module_title ">Contact Us</h3>

<div class="mod-custom mod-custom__"  >
	<h4>Toll Free: 1 800 987 65 43</h4>
<p>E-mail: <span id="cloakcfb505eea4e9c405d4482c510fc63f23">This email address is being protected from spambots. You need JavaScript enabled to view it.</span><script type='text/javascript'>
				document.getElementById('cloakcfb505eea4e9c405d4482c510fc63f23').innerHTML = '';
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				var addycfb505eea4e9c405d4482c510fc63f23 = '&#101;m&#97;&#105;l' + '&#64;';
				addycfb505eea4e9c405d4482c510fc63f23 = addycfb505eea4e9c405d4482c510fc63f23 + 'd&#101;m&#111;l&#105;nk' + '&#46;' + '&#111;rg';
				var addy_textcfb505eea4e9c405d4482c510fc63f23 = '&#101;m&#97;&#105;l' + '&#64;' + 'd&#101;m&#111;l&#105;nk' + '&#46;' + '&#111;rg';document.getElementById('cloakcfb505eea4e9c405d4482c510fc63f23').innerHTML += '<a ' + path + '\'' + prefix + ':' + addycfb505eea4e9c405d4482c510fc63f23 + '\'>'+addy_textcfb505eea4e9c405d4482c510fc63f23+'<\/a>';
		</script></p>
<h3>Our Address</h3>
<p>17600 Yonge Biggest St, Toronto<br /> Ontario L3Y 4Z1</p></div>
</div>
                        </div>
                                                                        <div class="footer-row-2">                    
                                                        <div class="footer-row-separator"></div>
                                                        <div class="moduletable   span9">

<div class="mod-custom mod-custom__"  >
	<p>DISCLAIMER: Aenean nec tortor erat. Nam metus sem, hendrerit rhoncus bibendum sit amet, vulputate et urna. Curabitur ac molestie nunc. Proin aliquam adipiscing quam eget auctor. In facilisis porta dui, sit amet scelerisque elit eleifend vel. Curabitur eget tortor in sapien tincidunt auctor. </p></div>
</div><div class="moduletable social  span3"><div class="mod-menu">
	<ul class="nav menu ">
	<li class="item-557"><a class="facebook" href="http://www.facebook.com" ><img src="/joomla-templates/002057/images/sampledata/asimages/social/facebook.png" alt="Facebook" /></a></li><li class="item-558"><a href="http://www.google.com" ><img src="/joomla-templates/002057/images/sampledata/asimages/social/google.png" alt="Google" /></a></li><li class="item-559"><a href="http://www.twitter.com" ><img src="/joomla-templates/002057/images/sampledata/asimages/social/twitter.png" alt="Twitter" /></a></li><li class="item-560"><a href="http://www.vimeo.com" ><img src="/joomla-templates/002057/images/sampledata/asimages/social/vimeo.png" alt="Vimeo" /></a></li><li class="item-561"><a href="http://www.rss.com" ><img src="/joomla-templates/002057/images/sampledata/asimages/social/rss.png" alt="Rss" /></a></li></ul>
</div>
</div>
                        </div>
                                            </div>
                </div>
            </div>
                            
            <div id="copyright-row">
                <div class="container">
                    <div id="trademark">
                        Copyright &copy; 2017 TECHNOWEB                    </div>
                    <div id="copyright-menu" class="row">
                        <div class="mod-menu">
	<ul class="nav menu ">
	<li class="item-502"><a href="/joomla-templates/002057/index.php/terms-of-use" >Terms of Use</a></li><li class="item-503"><a href="/joomla-templates/002057/index.php/privacy-policy" >Privacy Policy</a></li></ul>
</div>

                    </div>
                </div>
            </div>
            </div>
            
        </div>
    
    </div>
	<!-- END OF FOOTER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->    
    
        <div id="back-top">
    	<a href="#"><span></span></a>
    </div>
        
        
    
    <div class="mod-custom mod-custom__mobile_version">
        <p>
            <a href="http://www.astemplates.com/itempreview-tablet-landscape/212">
	            See <br>
            	Mobile <br>
            	Version
            </a>
        </p>
    </div>
            
    
    <script>
    
	var isInIFrame = (window.location != window.parent.location);

	if(isInIFrame == false)
	{
		<!-- Google Analytics -->
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
        ga('create', 'UA-17774394-1', 'auto');
        ga('send', 'pageview');
		
    	<!-- Alexa Certify Javascript -->
        _atrk_opts = { atrk_acct:"tD2of1a0CM002x", domain:"astemplates.com",dynamic: true};
        (function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
	}
	else
	{
		jQuery.noConflict();
		jQuery(document).ready(function()
		{
			jQuery(".mod-custom__mobile_version").hide();
		});
	}
    </script>  
    
</body>
</html>
