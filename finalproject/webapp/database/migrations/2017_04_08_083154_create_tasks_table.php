<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{

    public function up()
    {
        
        Schema::create('tasks', function (Blueprint $table) {
        $table->increments('id');
        $table->string('task_name');
        $table->datetime('task_due')->nullable();
        $table->boolean('task_status', false);
        $table->integer('user_id')->unsigned();
        $table->varchar(255)('task_hint');
        $table->text('task_info');
        $table->timestamps(); 
        
        });

    }

    public function down()
    {
        Schema::dropIfExists('tasks');///
    }
}
