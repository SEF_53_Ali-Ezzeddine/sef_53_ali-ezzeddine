<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubtasksTable extends Migration
{

    public function up()
    {

        Schema::create('subtasks', function (Blueprint $table) {
        $table->increments('id');
        $table->string('subtask_name');
        $table->boolean('task_status', false);
        $table->integer('task_id')->unsigned();
        $table->integer('user_id')->unsigned();
        $table->datetime('subtask_due');
        $table->timestamps();
        });
    }

    public function down()
    {
         
        Schema::dropIfExists('subtasks');
    }
}
