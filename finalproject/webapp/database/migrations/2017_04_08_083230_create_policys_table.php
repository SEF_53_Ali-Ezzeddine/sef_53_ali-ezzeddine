<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolicysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policys', function (Blueprint $table) {
        $table->increments('id');
        $table->string('pc_name');
        $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('policys');
    }
}
