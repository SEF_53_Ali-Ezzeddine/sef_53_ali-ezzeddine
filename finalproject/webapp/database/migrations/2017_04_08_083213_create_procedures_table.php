<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProceduresTable extends Migration
{

    public function up()
    {  
        Schema::create('procedures', function (Blueprint $table) {
        $table->increments('id');
        $table->string('pc_name');
        $table->integer('task_id')->unsigned();
        $table->integer('policy_id');
        $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('procedures');///
    }
}
