-- MySQL dump 10.13  Distrib 5.7.17, for Linux (i686)
--
-- Host: localhost    Database: webdata
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `members` (
  `m_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_name` varchar(255) NOT NULL,
  `m_category` varchar(255) NOT NULL,
  PRIMARY KEY (`m_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `members`
--

LOCK TABLES `members` WRITE;
/*!40000 ALTER TABLE `members` DISABLE KEYS */;
/*!40000 ALTER TABLE `members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policys`
--

DROP TABLE IF EXISTS `policys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policys` (
  `pc_id` int(11) NOT NULL AUTO_INCREMENT,
  `pc_name` varchar(255) NOT NULL,
  `pc_type` varchar(255) NOT NULL,
  PRIMARY KEY (`pc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policys`
--

LOCK TABLES `policys` WRITE;
/*!40000 ALTER TABLE `policys` DISABLE KEYS */;
/*!40000 ALTER TABLE `policys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=Inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'h','2006-02-14 22:04:36','2006-02-14 22:04:36',1),(2,'d','2006-02-14 22:04:36','2006-02-14 22:04:36',2);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `procedures`
--

DROP TABLE IF EXISTS `procedures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `procedures` (
  `proc_id` int(11) NOT NULL AUTO_INCREMENT,
  `proc_name` varchar(255) NOT NULL,
  `proc_created` datetime NOT NULL,
  `task_id` int(11) NOT NULL,
  PRIMARY KEY (`proc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `procedures`
--

LOCK TABLES `procedures` WRITE;
/*!40000 ALTER TABLE `procedures` DISABLE KEYS */;
/*!40000 ALTER TABLE `procedures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subtasks`
--

DROP TABLE IF EXISTS `subtasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subtasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subtask_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `subtask_due` datetime NOT NULL,
  `task_id` int(11) NOT NULL,
  `subtask_status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subtasks`
--

LOCK TABLES `subtasks` WRITE;
/*!40000 ALTER TABLE `subtasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `subtasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `task_due` datetime NOT NULL,
  `task_status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` VALUES (2,'Personal hygiene','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(3,'Diet and nutrition management','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(4,'Vitamin and supplement management','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(5,'Exercise regimen','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(6,'Stress ','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(7,'Sleep management appropriate for age or stage of development','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(9,'Safe sex practices','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(10,'Avoidance of smoking excessive alcohol consumption illicit drug use','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(12,'Use of protective equipment','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(13,'Regular medical and dental examinations screenings immunizations and care','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(14,'Medication management for minor illnesses','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(15,'First aid provision for minor injuries','2017-04-06 14:05:50','2017-04-07 14:25:31','2017-03-23 05:53:23',1),(16,'Wound care','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(17,'Burn care','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(18,'Recovery from serious injuries','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(19,'Recovery from major incidents heart attack stroke','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(20,'Recovery from surgeries','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(21,'Allergy treatment','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(22,'Pregnancy management and postpartum recovery','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(23,'Diabetes management','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(24,'Asthma management','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(25,'Apnea management','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(26,'Nutritional therapy','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(27,'Home infusion ','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(28,'Respiratory therapy','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(29,'Home dialysis','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(30,'Chronic obstructive pulmonary disease COPD care','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(31,'Tracheostomy care','2017-04-06 14:05:50','2017-04-06 22:52:13','2017-03-23 05:53:23',1),(32,'Decubitus ulcer pressure sore care','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(33,'Stoma  colostomy ileostomy ureterostomy care','2017-04-06 14:05:50','2017-04-06 14:05:50','2017-03-23 05:53:23',0),(34,'Catheterization and related care','2017-04-06 14:05:50','2017-04-07 14:25:23','2017-03-23 05:53:23',1),(35,'Rehabilitation regimens prescribed by physiatrists or physical','2017-04-06 14:05:50','2017-04-06 22:48:19','2017-03-23 05:53:23',1),(36,'Psychotherapeutic regimens','2017-04-06 14:05:50','2017-04-06 14:34:21','2017-03-23 05:53:23',1),(37,'Pain management','2017-04-06 14:05:50','2017-04-06 14:06:25','2017-03-23 05:53:23',1),(38,'Symptom management','2017-04-06 14:05:50','2017-04-06 14:08:14','2017-03-23 05:53:23',1),(39,'Care recipient and family counseling','2017-04-06 14:05:50','2017-04-06 15:36:39','2017-03-23 05:53:23',1);
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `tr_id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `proc_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `polict_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`tr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'ali','az@hotmail.com','$2y$10$ZDvEBEWTzdUHBz7Iulsu8OanDTaAhFJJmtWfqLS5humXyPXkFocgy','QKEc9ruwyjVj3m4dbjUcXeIkaN6lKg8ugMEkYB9BYoEc8dABTiuetsrLFFlO','2017-04-05 06:15:47','2017-04-05 06:15:47'),(2,'test','test@hotmail.com','$2y$10$qgwL26ZAB4gzj64grzSCV.3.n5rBJ0MCREI3JkWNekN2X.IziqZ4i',NULL,'2017-04-05 06:29:20','2017-04-05 06:29:20');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-08  1:43:51
