<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'TaskController@index');
Route::get('/calendar', 'TaskController@show');
Route::get('/list', 'SubtaskController@show');
Route::get('/listuser', 'SubtaskController@show');
Route::resource('/tasks', 'TaskController');
Route::resource('/subtasks', 'SubtaskController');

