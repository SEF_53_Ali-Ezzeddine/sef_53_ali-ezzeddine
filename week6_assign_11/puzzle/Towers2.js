

document.getElementsByTagName('title')[0].innerHTML = "Towers2";

var body = document.getElementsByTagName('body')[0];


var print = function ( text ) {
	
	if ( text === undefined )
		text = "";	

		body.innerHTML += text;
}

var println = function ( text ) {
		
	if ( text === undefined )
		
		text = "";	
	
		print( text + '</br>' );
}

String.prototype.format = function() {
	
	var result = this;
	
	for ( i = 0; i < arguments.length; i++ ) {
		
		var pattern = new RegExp("\\{" + i + "\\}", "g" );
		result = result.replace( pattern, arguments[i] );
	}
	
	return result;
}
/*var stack = function()
{
	 this.stac = new Array();
		 this.pop = function()
		{	
  			return this.stac.pop();
		}
 		this.push = function(item)
 		{
 		 this.stac.push(item);
		}
	}    */

var B2Poles=function( A,  C,  a, c)
{
    var pole1TopDisk = A.pop();
    var pole2TopDisk = C.pop();
 
 
    // When top disk of pole1 > top disk of pole2
     if (pole1TopDisk > pole2TopDisk)
    {
        A.push(pole1TopDisk);
        A.push(pole2TopDisk);
        moveDisk(c, a, pole2TopDisk);
    }
 
    // When top disk of pole1 < top disk of pole2
    else
    {
      	C.push(pole1TopDisk);
        C.push(pole2TopDisk);
        moveDisk(a, c, pole1TopDisk);
    }
}

var moveDisk=function(fromPeg,  toPeg,  disk)
{
    var text = "Move {0} from {1} to {2}".format( disk, fromPeg, toPeg );
	println( text );
}
	
var run=function() {
	var disks = 8;
 	var totalmoves = 254;

 
    // Create three stacks of size 'num_of_disks'
    // to hold the disks
    var stack = [];
    var A= new Array(disks);
    var B= new Array(disks);
 	var C= new Array(disks);
 	var a = 'A', c = 'C', b = 'B';
    //Larger disks will be pushed first
    for (var i = disks; i >= 1; i--) {
        A.push(i);
   }


/*
0  B  C
1  A  C
2  A  B

nb of discs even

try nbs from 1 to 254 
0 C  B
1 A  B
2 A  C

*/


    for (var i = 1; i <= totalmoves; i++)
    {

        if (i % 3 == 1) {

 

          B2Poles(A, B, a, b);
        }
 
        else if (i % 3 == 2) {
	
          B2Poles(A, C, a, c);
      }
 
        else if (i % 3 == 0)
        {
        
          B2Poles(C, B, c, b);
        }

    }

}

run();