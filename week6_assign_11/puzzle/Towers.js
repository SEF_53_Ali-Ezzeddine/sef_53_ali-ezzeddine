document.getElementsByTagName('title')[0].innerHTML = "Towers";

var body = document.getElementsByTagName('body')[0];

var print = function ( text ) {
	
	if ( text === undefined )
		text = "";	
	

		body.innerHTML += text;
}

var println = function ( text ) {
		
	if ( text === undefined )
		text = "";	
	
		print( text + '</br>' );
}

String.prototype.format = function() {
	
	var result = this;
	
	for ( i = 0; i < arguments.length; i++ ) {
		
		var pattern = new RegExp("\\{" + i + "\\}", "g" );
		result = result.replace( pattern, arguments[i] );
	}
	
	return result;
}



var run = function () {

	Hanoi(8);
}

function Hanoi(pieces) {
	
	solveHanoi( pieces, 1, 3 );
}

var stack = "ABC";
function solveHanoi( pieces, startStack, endStack ) {
	
	if ( pieces == 0 ) return;


	//middleStack=2
	var middleStack = 6 - startStack - endStack;
	solveHanoi( pieces - 1, startStack, middleStack );
	
	var text = "Move {0} from {1} to {2}".format( pieces, stack[ startStack - 1 ], stack[ endStack - 1 ] );
	println( text );
	
	solveHanoi( pieces - 1, middleStack, endStack );
}


run();











