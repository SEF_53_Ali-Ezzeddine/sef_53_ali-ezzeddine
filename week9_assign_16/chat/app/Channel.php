<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    
    protected $table = 'channels';

    /**
     * @var array
     */
    protected $fillable = array('name');

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany('Message', 'channel_id');
    }
}
