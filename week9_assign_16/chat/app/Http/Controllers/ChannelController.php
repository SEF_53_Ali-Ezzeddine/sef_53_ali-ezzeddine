<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Message;
use Illuminate\Http\Request;

class ChannelController extends Controller
{
    
      /*   * @return \Illuminate\Http\Response
     */

          /* @param ChatRoom $chatRooms
     */
    public function __construct(Channel $chatRooms)
    {
        $this->channels = $chatRooms;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        return $this->channels->all();
    }


    /**
     * @param ChatRoom $chatRoom
     * @return ChatRoom
     */



    /**
     * @return static
     */
    public function create()
    {
        return $this->channels->create(Input::all());
    }

   
    public function index()
    {
        $messages = Message::all();
        return view('channel.index', compact('messages'));
    }

    public function show($chid)
    {
        if(isset($chid)) {

            $messages = Message::all()->where('channel_id', $chid);
        }else{

            $messages = Message::all();
         }
        $data = array(
            'messages'=>$messages,
            'cmid'=>$chid
            ); 
       
           return view('channel.index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function edit(Channel $channel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Channel $channel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Channel $channel)
    {
        //
    }
}
