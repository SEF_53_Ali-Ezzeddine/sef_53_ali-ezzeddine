@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Chat Room !</div>

                <div class="panel-body">
                    You are logged in!

                </div>
                <i class="pe pe-7s-chat pe-2x pull-left pe-border"></i>
            </div>
        </div>
    </div>
</div>
@endsection
