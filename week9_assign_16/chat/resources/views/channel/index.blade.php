<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>

    html,body{font:normal 0.9em arial,helvetica;}
    #log {width:800px; height:430px; border:0.5px solid #7F9DB9; overflow:auto; margin:10px 0px 0px 0px;}
    #msg {width:650px;}
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    

    .navbar-header{
      padding-top: 0px;
      background-color: #f1f1f1;
      height: 100%;
      width: 100%;
    }
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #f1f1f1;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;
      } 
    }
  </style>
</head>
<body>

  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
                         
        </button>

                <a class="navbar-brand" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById(logout-form').submit();">
                  Logout<span class="glyphicon glyphicon-log-in"></span>
               </a>


      </div>
    </div>
  </nav>
  
  <div class="container-fluid text-center">    
    <div class="row content">


      <div class="col-sm-10 text-left" align="left"> 

      <head>
        
          <title>Simple Chat</title>

          <script>
              var socket;


              function createSocket(host) {

                  if ('WebSocket' in window)
                      return new WebSocket(host);
                  else if ('MozWebSocket' in window)
                      return new MozWebSocket(host);

                     throw new Error("No web socket support in browser!");
              }

              function init() {
                  var host = "ws://localhost:12345/chat";
                  try {
                      socket = createSocket(host);
                     // log('WebSocket - status ' + socket.readyState);
                      socket.onopen = function(msg) {
                      //    log("Welcome - status " + this.readyState);
                      };
                      socket.onmessage = function(msg) {
                        log(msg.data);
                      };
                      socket.onclose = function(msg) {
                      //    log("Disconnected - status " + this.readyState);
                      };
                  }
                  catch (ex) {
                      log(ex);
                  }
                  document.getElementById("msg").focus();
              }

              function send(id,cid) {
                  var msg = document.getElementById('msg').value;
                  msg += "-"+id+"-"+cid;

                  try {

                      socket.send(msg);

                  } catch (ex) {
                      log(ex);
                  }
              }
              function quit() {
                  log("Goodbye!");
                  socket.close();
                  socket = null;
              }

              function log(msg) {
                  document.getElementById("log").innerHTML += "<br><br>" + msg;

                      var objDiv = document.getElementById("log");
                      objDiv.scrollTop = objDiv.scrollHeight;
              }
              function onkey(event) {
                  if (event.keyCode == 13) {
                      send();
                  }
              }
          </script>

    </head>

        
      <body onload="init()">
          <div id="log" class="form-control">
              
              @foreach($messages as $message)
               
                <div>
                <hr>
                 <th>{{ $message->user()->get()[0]->name }}</th>
                  <p class="well">{{ $message->body }}</p>
                </div>
                    
              @endforeach
          </div>
          <hr>
          <input id="msg" type="text" class="cleardefault  onkeypress="onkey(event)">
          <button onclick="send({{ Auth::id()}}, {{ $cmid }})">Send</button>
          <button onclick="quit()">Quit</button>
     
      </body>

      </div>
      <div class="col-sm-2 sidenav">


            <p><div class="well"><a  href="{{ route ('channels.show',['chid'=>'1'])}}">
            General</div></p>
            <p><div class="well"><a  href="{{ route ('channels.show',['chid'=>'2'])}}">
            Music</div></p>
            <p><div class="well"><a  href="{{ route ('channels.show',['chid'=>'3'])}}">
            Assignments</div></p>

           

      </div>

       
           
    </div>
  </div>



</body>
</html>