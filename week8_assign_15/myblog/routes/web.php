<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('blog/home');
//});

Route::get('/', 'PostController@publicHomePage');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('posts','PostController');

//Route::get('posts/indexsp', 'PostController@indexsp');
Route::get('posts/indexsp', [ 'uses' => 'PostController@indexsp', 'as' => 'posts.indexsp']); 

//Route::resource('comments','CommentController');
//Route::get('users/{id}', 'UserController@getProfile')->where('id', '[\d+]+');

Route::POST('comments/{post_id}', [ 'uses' => 'CommentController@store', 'as' => 'comments.store']); 

Route::POST('likes/{post_id}', [ 'uses' => 'LikeController@store', 'as' => 'likes.store']); 

Route::get('comments/{post_id}', [ 'uses' => 'CommentController@show', 'as' => 'comments.show']); 