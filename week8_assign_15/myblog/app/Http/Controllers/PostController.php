<?php

namespace myblog\Http\Controllers;

use myblog\Post;
use myblog\comments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Auth;


//use Intervention\Image\ImageManagerStatic as Image;﻿

class PostController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */


  public function publicHomePage()
  {
    $posts = Post::paginate(5);
    return view('blog/home', ['posts'=>$posts]);
  }
  
  public function index()
  {
    $loggedUserId = Auth::id();
    $posts = Post::all()->where('user_id', $loggedUserId);
    return view('adminPanel/home', ['posts'=>$posts]);
  }


  
  public function indexsp()
  {

    $loggedUserId = Auth::id();
    $posts = Post::all()->where('user_id', $loggedUserId);
    dd($posts);
    return view('blog/home', ['posts'=>$posts]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('adminPanel/create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {

 //   $this->validate($request, [
//    'body' => 'required',
 //   ]);


    $post = new Post;
    $postUserId = Auth::id();
    $user = Auth::user();



      if($request->hasFile('image')){

          $photo = $request->file('image');
          $filename = time() . $photo->getClientOriginalName();
          $upload = 'images/'.$user->name.'-'.$user->id.'/';
          $postImageName = $upload.$photo->getClientOriginalName();
          //Image::make($photo)->resize(800,400);
          $post->image = $filename;
          storage::put($filename, file_get_contents($photo->getRealPath()));
  }



    $caption = $request->textline;
    $post->caption = $caption;
    $post->user_id = $postUserId;
    $post->uname = $user->name;
//    $post->image = $postImageName;
    $post->likes = 0;
    $post->comments = 0;

    $post->save();

    return redirect()->route('posts.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  \myblog\Post  $post
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {

    $data = array(
        'id' => $id,
        'post' => $post
    );

    return view('blog.view_post', $data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \myblog\Post  $post
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $post = Post::find($id);

    return view('adminPanel.edit', ['post'=>$post]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \myblog\Post  $post
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $post = Post::find($id);
    $post->title = $request->title;
    $post->body = $request->body;

    $post->save();

    return redirect()->route('posts.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \myblog\Post  $post
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $post = Post::find($id);
    $post->delete();
    return redirect()->route('posts.index');
  }
}
