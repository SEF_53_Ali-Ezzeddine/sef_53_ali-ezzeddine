<?php

namespace myblog\Http\Controllers;

use Illuminate\Http\Request;
use myblog\comments;
use myblog\likes;
use myblog\Post;
use Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $post_id)
    {


    $comment = new comments;
    $post= Post::find($post_id);
    $commentUserId = Auth::id();
    //$postId= Auth::user();
     $comment->user_id = Auth::id();
   // $comment->associate($post);
    $comment->comment = $request->comment;
    $comment->post_id =$post_id;
    $comment->save();
    return redirect()->route('posts.index');
    }



    public function storeLike(Request $request, $post_id)
    {


    $like = new likes;
    $post= Post::find($post_id);

    $like->user_id = Auth::id();
    $like->post_id =$post_id;
    $like->save();
    return redirect()->route('posts.index');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      //  $posts = DB::table('posts')
        //    ->join('comments', 'posts.id', '=', 'comments.post_id')
            
          //  ->select('posts.*', 'comments.comment')
           // ->get();
   // $post = comments::find($id);
   // $data = array(
   //     'posts.id' => $id,
   //     'comments.comment' => $post
   // );

    //return view('blog.view_post', $data);
    }








  
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
