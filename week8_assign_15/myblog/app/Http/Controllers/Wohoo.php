<?php

namespace myblog\Http\Controllers;

use Illuminate\Http\Request;
use myblog\Http\Requests;

class Wohoo extends Controller
{
    /**
    *
    *@return Response
    */
    public function index()
    {
    	$users = DB::table('users')->get();

        return view('user.index', ['users' => $users]);
    }
}
