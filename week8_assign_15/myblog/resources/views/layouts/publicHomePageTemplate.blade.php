<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>

  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <title>@yield('title')</title>
</head>

<body>

  <div class="container">
    <div class="loginBox nav navbar-nav pull-right">
      @if (Auth::guest())
      <li><a href="index.php/login">Login</a></li>
      <li><a href="index.php/register">Register</a></li>
      @else
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        {{ Auth::user()->name }} <span class="caret"></span>
        </a>

        <ul class="dropdown-menu" role="menu">
          <li>
            <a href="index.php/logout" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">Logout</a>

            <form id="logout-form" action="index.php/logout" method="POST" style="display: none;">
            {{ csrf_field() }}
            </form>
          </li>
        </ul>
      </li>
      @endif
    </div>  

    <div class="row headingBox">
    
      <div>
        <h1><a href="{{ url('/') }}"></a></h1><br>
      </div>

    <nav class="navbar navbar-default">
      <div class="container-fluid">

       @if(Auth::check())
       <ul class="nav navbar-nav navbar-right">
   
         <li><a href="index.php/posts"><i class="glyphicon glyphicon-envelope" style="font-size:48px;color:red;"></i> </a></li>
       </ul>
       @endif
      </div>
    </nav>

    <div>
      @yield('content')
    </div>

    <div class="footer text-center" style="margin: 20px 0 60px 0;">
  
    </div>
  </div>

</body>
</html>
