@extends('layouts.template')

@section('title','Edit Post #' . $post->id)

@section('content')
<h1># {{ $post->id }}</h1>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<div class="col-sm-8 col-sm-offset-2">


  <form action="{{ route('comments.store', $post->id) }}" method="post">

 
  {{ csrf_field() }}


    
    <div class="form-group">

    </div>

    <div class="form-group">

      <textarea name="comment" class="form-control" cols="10" rows="10" required="required" style="height: 80px ;">
      </textarea>
    </div>




    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="../" class="btn btn-default pull-right">Go Back</a>
  </form>

<hr>


    <form action="{{ route('likes.store', $post->id) }}" method="post">

 
  {{ csrf_field() }}

    <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-thumbs-up" style="font-size:25px;color:red;"></i></button>

   
        <a href="#">
          <span class="glyphicon glyphicon-thumbs-down" style="font-size:15px;"></span>
        </a>
    
<hr>    

  </form>

      <form action="{{ route('comments.show', $post->id) }}" method="post">


  {{ csrf_field() }}
              <tbody>
          
      @foreach($post->comment as $comment1)
    <div class="alert alert-danger">

      <p>{{ $comment1->comment }}</p>
      <a href="{{ route('comments.show', ['id'=>$post->id]) }}" ></a>
     
    </div>
    @endforeach

        </tbody>


  </form>


</div>

@endsection






