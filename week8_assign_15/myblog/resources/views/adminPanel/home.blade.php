@extends('layouts.template')

@section('title','')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="loginBox nav navbar-nav pull-right">
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> {{ Auth::user()->name }} <span class="caret"></span>
    </a>

    <ul class="dropdown-menu" role="menu">
      <li>
        <a href="logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>

        <form id="logout-form" action="logout" method="POST" style="display: none;"> {{ csrf_field() }}
        </form>
      </li>
    </ul>
  </li>
</div>

<hr>
<hr>
<a href="{{ url('/') }}" class="btn btn-primary pull-left"> <i class="fa fa-arrow-circle-o-left" style="font-size:36px"></i></a>

<a href="posts/create" class="btn btn-primary pull-right"> + </a>

<hr>
<hr>
<br><br><br>
<table class="table">

  <tbody>
    @foreach($posts as $post)
    <tr>
      <th>{{ $post-> id }}</th>

      <td>

        <div class="alert alert-info" align="center">{{ $post->caption }}</div>
      <img src="../../storage/app/{{ $post->image}}" border=3 height=400 width=300>
      
      </td>

      <td><a href="{{ route('posts.edit', ['id'=>$post->id]) }}" class="btn btn-info"><i class="fa fa-newspaper-o" style="font-size:36px"></i>

      </a>

      </td>
      
      <td>
      <form action="{{ route('posts.destroy', ['id'=>$post->id]) }}" method="post">
      {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
        <i class="fa fa-trash"></i>
        <input class="btn btn-danger" type="submit" value="-">
      </form>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>

@endsection