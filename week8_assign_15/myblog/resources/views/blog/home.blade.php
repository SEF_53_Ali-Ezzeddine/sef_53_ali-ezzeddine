@extends('layouts.publicHomePageTemplate')

@section('title', 'Blog Public Home Page')

@section('content')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <div>


    <div class="row text-center">
      {{ $posts->links() }}
    </div>
  </div>

<hr>
<br><br>
<table class="table">

  <tbody>
    @foreach($posts as $post)
    <tr>
      <th style="font-size:25px;"><i class="glyphicon glyphicon-user">{{ $post-> uname }}</th>


      <td><img src="../storage/app/{{ $post->image}}" border=10 height=400 width=300 class="btn btn-default pull-right"> <div class="alert alert-info" align="center">{{ $post->caption }}</div><br>
      
      <div class="alert alert-info" >Likes: {{ count($post->likes()->get()) }}<br><br>Comments: {{ count($post->comment()->get()) }}</div>

      </td>
     

      <td><a href="{{ route('posts.edit', ['id'=>$post->id]) }}" class="btn btn-info"><i class="fa fa-newspaper-o" style="font-size:36px"></i></a><br><br><br></td>
      
      <td>
      <form action="{{ route('posts.destroy', ['id'=>$post->id]) }}" method="post">
      {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">

      </form>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>

@endsection